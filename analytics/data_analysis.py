import math
import pandas as pd
import pickle
import numpy as np
import os
import urllib
import matplotlib.pyplot as plt
import sklearn
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from sklearn.calibration import CalibratedClassifierCV
from sklearn.linear_model import LogisticRegression

DTYPES = {
    'Accounts.AccountCategory' : "category",
    'Accounts.AccountRefDay' : float,
    'Accounts.AccountRefMonth' : str,
    'Accounts.LastMadeUpDate' : str,
    'Accounts.NextDueDate' : str,
    'CompanyCategory' : "category",
    'CompanyName' : str,
    'CompanyNumber' : str,
    'CompanyStatus' : "category",
    'ConfStmtLastMadeUpDate' : str,
    'ConfStmtNextDueDate' : str,
    'CountryOfOrigin' : "category",
    'DissolutionDate' : str,
    'IncorporationDate' : str,
    'LimitedPartnerships.NumGenPartners' : int,
    'LimitedPartnerships.NumLimPartners' : int,
    'Mortgages.NumMortCharges' : int,
    'Mortgages.NumMortOutstanding' : int,
    'Mortgages.NumMortPartSatisfied' : int,
    'Mortgages.NumMortSatisfied' : int,
    'PreviousName_1.CONDATE' : str,
    'PreviousName_1.CompanyName' : str,
    'PreviousName_10.CONDATE' : str,
    'PreviousName_10.CompanyName' : str,
    'PreviousName_2.CONDATE' : str,
    'PreviousName_2.CompanyName' : str,
    'PreviousName_3.CONDATE' : str,
    'PreviousName_3.CompanyName' : str,
    'PreviousName_4.CONDATE' : str,
    'PreviousName_4.CompanyName' : str,
    'PreviousName_5.CONDATE' : str,
    'PreviousName_5.CompanyName' : str,
    'PreviousName_6.CONDATE' : str,
    'PreviousName_6.CompanyName' : str,
    'PreviousName_7.CONDATE' : str,
    'PreviousName_7.CompanyName' : str,
    'PreviousName_8.CONDATE' : str,
    'PreviousName_8.CompanyName' : str,
    'PreviousName_9.CONDATE' : str,
    'PreviousName_9.CompanyName': str,
    'RegAddress.AddressLine1' : str,
    'RegAddress.AddressLine2' : str,
    'RegAddress.CareOf' : str,
    'RegAddress.Country' : str,
    'RegAddress.County' : str,
    'RegAddress.POBox' : str,
    'RegAddress.PostCode' : str,
    'RegAddress.PostTown' : str,
    'Returns.LastMadeUpDate' : str,
    'Returns.NextDueDate' : str,
    'SICCode.SicText_1' : str,
    'SICCode.SicText_2' : str,
    'SICCode.SicText_3' : str,
    'SICCode.SicText_4' : str,
    'URI' : str
}

DATE_FIELDS = [
    'Accounts.LastMadeUpDate',
    'Accounts.NextDueDate',
    'ConfStmtLastMadeUpDate',
    'ConfStmtNextDueDate',
    'DissolutionDate',
    'IncorporationDate',
    'PreviousName_1.CONDATE',
    'PreviousName_10.CONDATE',
    'PreviousName_2.CONDATE',
    'PreviousName_3.CONDATE',
    'PreviousName_4.CONDATE',
    'PreviousName_5.CONDATE',
    'PreviousName_6.CONDATE',
    'PreviousName_7.CONDATE',
    'PreviousName_8.CONDATE',
    'PreviousName_9.CONDATE',
    'Returns.LastMadeUpDate',
    'Returns.NextDueDate'
]

COMPANIES_HOUSE_URL = 'http://download.companieshouse.gov.uk'

def fetch_data(url, path):
    if not os.path.isfile(path):
        try:
            urllib.request.urlretrieve(url, path)
        except Exception as e:
            print("exception while trying to fetch %s" % url)
            raise(e)

def extract_data(suffix, n_rows=None):
    path = "BasicCompanyData%s" % suffix
    file_name = path + '.zip'
    fetch_data(COMPANIES_HOUSE_URL + '/' + file_name, file_name)
    frame = pd.read_csv(file_name, sep=',', nrows=n_rows,
                        dtype=DTYPES, parse_dates=DATE_FIELDS,
                        skipinitialspace=True)
    ref_day = 'Accounts.AccountRefDay'
    frame.loc[ref_day] = frame[ref_day].fillna(0).astype(np.uint8)
    return frame

def compare_months(df_before, df_after):
    was_active = (df_before.CompanyName.isin(df_after.CompanyName)) & \
                 (df_before.CompanyStatus == 'Active')
    previously_active = df_before[was_active]
    now_bad = df_after[df_after.CompanyStatus != 'Active'].CompanyName
    previously_active.loc[:, 'TurnedBad'] = previously_active.CompanyName.isin(now_bad)
    return previously_active

def prepare_data_for_ml(df):
    X = pd.DataFrame()
    numeric_fields = [
        'Accounts.LastMadeUpDate',
        'Accounts.NextDueDate',
        'IncorporationDate',
        'LimitedPartnerships.NumGenPartners',
        'LimitedPartnerships.NumLimPartners',
        'Mortgages.NumMortCharges',
        'Mortgages.NumMortOutstanding',
        'Mortgages.NumMortPartSatisfied',
        'Mortgages.NumMortSatisfied',
        'Returns.LastMadeUpDate',
        'Returns.NextDueDate'
    ]
    categorical_fields = [
        'Accounts.AccountCategory',
        'CompanyCategory',
        'CountryOfOrigin',
        'RegAddress.PostTown',
        'SICCode.SicText_1',
        'SICCode.SicText_2',
        'SICCode.SicText_3',
        'SICCode.SicText_4'
    ]
    name_changes = np.zeros(len(df))
    for i in range(1, 11):
        not_present = df['PreviousName_%d.CONDATE' % i].isnull()
        changed = 1 - not_present
        name_changes += changed
    X['NameChanges'] = name_changes
    for field in numeric_fields:
        X[field] = pd.to_numeric(df[field], downcast='float')
    for field in categorical_fields:
        X[field] = df[field]
    X['PostCode'] = df['RegAddress.PostCode'].str.slice(0, 2)
    categorical_fields.append('PostCode')
    X['AccountsDelta'] = X['Accounts.NextDueDate'] - X['Accounts.LastMadeUpDate']
    X['ReturnsDelta'] = X['Returns.NextDueDate'] - X['Returns.LastMadeUpDate']
    X = pd.get_dummies(X, columns=categorical_fields, sparse=True)
    Y = np.array(df['TurnedBad'], dtype='uint8')
    return (X, Y)

def test_train_split(X, Y, p_train=0.8, seed=0):
    if seed is not None:
        np.random.seed(seed)
    train_mask = np.random.rand(len(X)) < p_train
    test_mask = ~train_mask
    X_train, X_test = X[train_mask], X[test_mask]
    Y_train, Y_test = Y[train_mask], Y[test_mask]
    return (X_train, Y_train, X_test, Y_test)

def train_ml(X_train, Y_train,
             model_type=GradientBoostingClassifier,
             verbose=1):
    #Allow for either a class or an instance to be passed as the model type
    try:
        classifier = model_type(verbose=verbose)
    except TypeError:
        classifier = model_type
    classifier.fit(X_train, Y_train)

    return classifier

def evaluate_classifier(classifier, X_train, Y_train, X_test, Y_test, plot_graph=True):

    true_mask = Y_test.astype(bool)
    Y_probs = classifier.predict_proba(X_test)
    Y_pred = classifier.predict(X_test)
    true_probs = Y_probs[true_mask][:, 1]
    false_probs = Y_probs[~true_mask][:, 1]
    print("Mean predicted failure probability for still-active companies:")
    print(false_probs.mean())
    print("Mean predicted failure probability for failed companies:")
    print(true_probs.mean())
    print("Confusion Matrix:")
    print(sklearn.metrics.confusion_matrix(Y_pred, Y_test))
    print("Log Loss:")
    print(sklearn.metrics.log_loss(Y_test, Y_probs))

    #Print feature importances if supported by the given classifier
    try:
        importances = classifier.feature_importances_
        print("Feature Importances:")
        for t in sorted(zip(importances, X.keys()), reverse=True):
            importance, _ = t
            if importance == 0.:
                break
            print(t)
    except AttributeError:
        pass

    if plot_graph:
        plt.subplot(211)
        plt.title("Still-Active Companies")
        plt.xlabel("Predicted Failure Probability")
        plt.ylabel("Relative Frequency")
        plt.hist(false_probs,
                 bins = 'auto',
                 facecolor='green',
                 alpha=0.75)
        plt.subplot(212)
        plt.title("Failed Companies")
        plt.xlabel("Predicted Failure Probability")
        plt.ylabel("Relative Frequency")
        plt.hist(true_probs,
                 bins = 'auto',
                 facecolor='red',
                 alpha=0.75)
        plt.tight_layout()
        plt.savefig('prediction_accuracy.png')
        plt.show()

    return classifier

def main(old_file = "AsOneFile-2018-07-01",
         new_file = "AsOneFile-2018-10-01",
         model_type=GradientBoostingClassifier,
         n_rows=None,
         plot_graph=True,
         model_path=None):

    df_before = extract_data(old_file, n_rows=n_rows)
    df_after = extract_data(new_file, n_rows=n_rows)
    df_comparison = compare_months(df_before, df_after)

    n, k = len(df_comparison), df_comparison.TurnedBad.sum()
    print("%d of %d (%.2f%%) companies turned bad" % (k, n, (100. * k) / n))

    X, Y = prepare_data_for_ml(df_comparison)
    X_train, Y_train, X_test, Y_test = test_train_split(X, Y)

    classifier = train_ml(X_train, Y_train, model_type=model_type)

    evaluate_classifier(classifier, X_train, Y_train, X_test, Y_test, plot_graph=plot_graph)

    if model_path is not None:
        pickle.dump(classifier, model_path)

if __name__ == "__main__":
    main(model_type=CalibratedClassifierCV(GradientBoostingClassifier(), cv=5),
         old_file="-2018-07-01-part1_5",
         new_file="-2018-10-01-part1_6",
         n_rows=1000)
