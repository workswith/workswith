const debug = require('debug')('api')
const { ApolloServer, gql, makeExecutableSchema} = require('apollo-server')

/**
 * Get the schema definition:
 */
const { typeDefs, typeResolvers } = require('./schema')

/**
 * Get the datasource definitions:
 */
const dataSources = require('./DataSources')

/**
 * Load resolvers that know how to navigate the data, but use the provided
 * datasources to get the data:
 */

const organizationResolver = require('./OrganizationResolver')
const postCodeUnitResolver = require('./PostCodeUnitResolver')
const predictionResolver = require('./PredictionResolver')

class Api {
  constructor() {
    /**
     * Set up the schema and wire in to the resolvers:
     */
    const resolvers = {
      Query: {
        ...organizationResolver.query,
        ...postCodeUnitResolver.query,
        ...predictionResolver.query
      },
      ...organizationResolver.type,
      ...postCodeUnitResolver.type,
      ...predictionResolver.type,
      ...typeResolvers
    }
    const schema = makeExecutableSchema({
      typeDefs,
      resolvers
    })

    /**
     * Create an Apollo Server that uses the schemas and
     * the datasources:
     */
    this.api = new ApolloServer({
      schema,
      dataSources,
      introspection: true,
      playground: true
    })
  }

  close() {
    return this.server && this.server.close()
  }

  async listen() {
    const { url, server } = await this.api.listen()
    this.server = server
    console.log(`🚀  GraphQL API ready at ${url}`);
    return { url, server }
  }
}

module.exports = Api
