const tf = require('@tensorflow/tfjs')
require('@tensorflow/tfjs-node')

const {
  HYPER_LEARNING_RATE,
  HYPER_LOSS,
  MODEL_FILE
} = process.env

const { featureColumns } = require('./normalise')

class PredictionDataSource {
  get query() {
    return {
      Prediction: async args => {
        const modelFile = MODEL_FILE
        /**
         * Load the model:
         */
        const model = await tf.loadModel(modelFile)
        const optimizer = tf.train.adam(+HYPER_LEARNING_RATE)
        model.compile({
          optimizer,
          loss: HYPER_LOSS,
          metrics: ['accuracy']
        })

        /**
         * Establish the feature columns for the inputs:
         */
        const features = featureColumns(args)
        const predictXs = tf.tensor2d([features[1]])
        const prediction = model.predict(predictXs)
        const argMax = await prediction.argMax(1).data()
        const data = await prediction.data()
        const risk = data[1]

        return {
          risk: Math.floor(risk * 100)
        }
      }
    }
  }

  get type() {
    return {
      Prediction: class Prediction {
        constructor(prediction) {
          this.risk = prediction.risk
        }
      }
    }
  }
}

module.exports = PredictionDataSource
