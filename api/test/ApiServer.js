const tap = require('tap')
tap.comment('API for Schema.org representation of Companies House data')

const { gql } = require('apollo-server')
const ApiClient = require('../app/ApiClient')
const ApiServer = require('../app/ApiServer')

tap.test('query for Engine House', async t => {
  /**
   * Create an API server. It should default to listening
   * on port 4000:
   */
  const api = new ApiServer()
  const { url } = await api.listen()
  t.same(url, 'http://localhost:4000/')

  /**
   * Create a client to query the server:
   */
  const client = new ApiClient(url)

  /**
   * Check we can get a company back:
   */
  const data = await client.query({
    query: gql`
      {
        Organization(identifier: "07716384") {
          legalName,
          identifier
        }
      }
    `
  })
  t.same(
    data.data,
    {
      Organization: [{
        __typename: 'Organization',
        legalName: 'IDEA IS EVERYTHING LIMITED',
        identifier: '07716384'
      }]
    }
  )
  api.close()
})
