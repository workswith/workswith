const orgCategoryMap = require('./orgCategory')
const orgRurIndMap = require('./orgRurInd')
const orgStatusMap = require('./orgStatus')
const orgTypeMap = require('./orgType')

const featureColumns = args => {
  /**
   * Create one hot values:
   */
  const age = +args.orgAge
  const category = orgCategoryMap.oneHot(orgCategoryMap.normalise(args.orgCategory))
  const employees = +args.orgEmployees
  const profitAndLoss = +args.orgProfitAndLoss
  const rural = orgRurIndMap.oneHot(orgRurIndMap.normalise(args.rurInd))
  const type = orgTypeMap.oneHot(orgTypeMap.normalise(args.orgType))

  return [undefined, [].concat(age).concat(category).concat(employees).concat(profitAndLoss).concat(rural).concat(type)]
}

exports.featureColumns = featureColumns
