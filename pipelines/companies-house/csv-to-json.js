const path = require('path')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/transforms/DoFn')
const ParDo = require('beamish-direct-runner/lib/sdk/transforms/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/Pipeline')
const TextIO = require('beamish-direct-runner/lib/sdk/io/TextIO')

class ToJsonFn extends DoFn {
  apply(input) {
    return JSON.stringify(input)
  }
}

const p = Pipeline.create()

p
.apply(
  TextIO.read()
  .from(path.resolve(__dirname, '../../data/data/dat-data/companies-house/BasicCompanyData/2018-03-01/csv/part1_5.csv'))
)
.apply(ParDo.of(new Csv(true)))
.apply(ParDo.of(new ToJsonFn()))
.apply(
  TextIO.write()
  .to(path.resolve(__dirname, '../../data/data/dat-data/companies-house/BasicCompanyData/2018-03-01/json/part1_5.json'))
)

p.run().waitUntilFinish()