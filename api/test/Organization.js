const tap = require('tap')
tap.comment('query for Schema.org/Organization')

const { ApolloServer, gql, makeExecutableSchema} = require('apollo-server')
const { ApolloClient } = require('apollo-client')
const { InMemoryCache } = require('apollo-cache-inmemory')
const { HttpLink } = require('apollo-link-http')
const fetch = require('node-fetch')

const client = new ApolloClient({
  link: new HttpLink({ uri: 'http://localhost:4000', fetch }),
  cache: new InMemoryCache()
})

tap.test('legalName', async t => {
  const typeDefs = gql`
    type Query {
      Organization: [Organization]
    }

    type Organization {
      "The official name of the organization, e.g. the registered company name."
      legalName: String
    }
  `

  const resolvers = {
    Query: {
      Organization: () => ([{}])
    },
    Organization: {
      legalName: () => 'ENGINE HOUSE SOFTWARE LIMITED'
    }
  }

  const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
  })

  const api = new ApolloServer({ schema })

  const { url, server } = await api.listen()
  t.same(url, 'http://localhost:4000/')

  const data = await client.query({ query: gql`{ Organization { legalName } }` })
  t.same(
    data.data,
    {
      Organization: [
        {
          __typename: 'Organization',
          legalName: 'ENGINE HOUSE SOFTWARE LIMITED'
        }
      ]
    }
  )
  server.close()
})

tap.test('identifier', async t => {
  const typeDefs = gql`
    type Query {
      Organization: [Organization]
    }

    type Organization {
      "The official name of the organization, e.g. the registered company name."
      legalName: String
      "The identifier property represents any kind of identifier for any kind of Thing, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links."
      identifier: String
    }
  `

  const resolvers = {
    Query: {
      Organization: () => ([{}])
    },
    Organization: {
      legalName: () => 'ENGINE HOUSE SOFTWARE LIMITED',
      identifier: () => '09426790'
    }
  }

  const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
  })

  const api = new ApolloServer({ schema })

  const { url, server } = await api.listen()
  t.same(url, 'http://localhost:4000/')

  const data = await client.query({ query: gql`{ Organization { legalName, identifier } }` })
  t.same(
    data.data,
    {
      Organization: [{
        __typename: 'Organization',
        legalName: 'ENGINE HOUSE SOFTWARE LIMITED',
        identifier: '09426790'
      }]
    }
  )
  server.close()
})
