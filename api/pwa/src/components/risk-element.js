import { html, LitElement } from '@polymer/lit-element';

import { SharedStyles } from './shared-styles.js';
import '../../node_modules/ag-grid-community/dist/ag-grid-community.min.noStyle.js';
import '../../node_modules/ag-grid-webcomponent/src/agGrid.js';

const monthCellClassRules = {
    'no-risk': 'x === \'none\'',
    'very-low-risk': 'x === \'very low\'',
    'low-risk': 'x === \'low\'',
    'medium-risk': 'x === \'medium\'',
    'high-risk': 'x === \'high\''
};

export class RiskElement extends LitElement {
  constructor() {
    super()

    this.columnDefs = [
      {
        headerName: 'Company Name',
        field: 'name'
      },
      {
        headerName: 'Status',
        field: 'status'
      },
      {
        headerName: 'Risk Level',
        field: 'riskLevel',
        cellClassRules: monthCellClassRules
      },
      {
        headerName: 'Risk Factor',
        field: 'risk'
      },
    ];

    this.rowData = [];
  }

  firstUpdated() {
    const gridOptions = {
      columnDefs: this.columnDefs,
      rowData: this.rowData,
      onFirstDataRendered: function () {
        gridOptions.api.sizeColumnsToFit()
      },
      async onGridReady() {
        const response = await fetch('/src/components/companies-predict.json')
        this.api.setRowData(await response.json())
      }
    }
    const agGrid = this.shadowRoot.getElementById('agGrid')
    agGrid.gridOptions = gridOptions
  }

  render() {
    return html`
      ${SharedStyles}
      <style>
        ag-grid {
          padding: 12px;
          border: solid gainsboro 1px;
          height: 540px;
        }
      </style>
      <link rel='stylesheet' href='../../node_modules/ag-grid-community/dist/styles/ag-grid.css'>
      <link rel='stylesheet' href='../../node_modules/ag-grid-community/dist/styles/ag-theme-balham.css'>
      <link rel='stylesheet' href='/src/components/style-risk.css'>
      <ag-grid
       id='agGrid'
       class='ag-theme-balham'
       enableFilter
       enableSorting
      ></ag-grid>
    `
  }
}

window.customElements.define('risk-element', RiskElement)
