## Possible Ontology Term Mapping for Companies House Data

Companies House Fields taken from:

http://resources.companieshouse.gov.uk/toolsToHelp/pdf/freeDataProductDataset.pdf

### Namespaces
* prefix adms: \<http://www.w3.org/ns/adms#>
* prefix terms:\<http://business.data.gov.uk/companies/def/terms/>
* prefix dct: \<http://purl.org/dc/terms/>
* prefix org: \<http://www.w3.org/ns/org#>
* prefix owl: \<http://www.w3.org/2002/07/owl#>
* prefix postcode: \<http://data.ordnancesurvey.co.uk/ontology/postcode/>
* prefix rov: \<http://www.w3.org/ns/regorg#>
* prefix rdf: \<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
* prefix rdfs: \<http://www.w3.org/2000/01/rdf-schema#>
* prefix skos: \<http://www.w3.org/2004/02/skos/core#>
* prefix sic: \<http://business.data.gov.uk/companies/def/sic-2007/>
* prefix text: \<http://jena.apache.org/text#>
* prefix vcard: \<http://www.w3.org/2006/vcard/ns#>
* prefix xsd: \<http://www.w3.org/2001/XMLSchema#>

* prefix schema: \<http://schema.org/>

Table entries in **_Bold Italics_** aren't explicit fields, but sections in the CH fields.

'Type' : C = Class; P,L = Property with a Literal object; P,R = Property with Resource object

**TODO**
terms:companyProfile
rov:registration

| Companies House Field                     | Max. size | Type | CH Linked Data Terms        | Alternatives               |
|-------------------------------------------|-----------|------|-----------------------------|----------------------------|
| **Company**                               |           | C    | org:FormalOrganization      | **schema:LocalBusiness**   |
|                                           |           |      | rov:RegisteredOrganization  | **schema:Organization**    |
|                                           |           |      | terms:RegisteredCompany     | (schema:url)               |
|                                           |           |      |                             |                            |
| CompanyName                               | 160       | P,L  | skos:prefLabel              | schema:name                |
|                                           |           |      |                             | schema:legalName           |
|                                           |           |      | rov:legalName               |                            |
| CompanyNumber                             | 8         | P,L  | skos:notation               | schema:identifier          |
|                                           |           |      | org:identifier              |                            |
| **_Registered Office Address_**           |           | P,R  | terms:registeredAddress     | **schema:PostalAddress**   |
|                                           |           |      |                             | schema:address             |
|                                           |           |      |                             | schema:location            |
| Careof                                    | 100       |      |                             |  *TODO*                    |
| POBox                                     | 10        |      |                             | schema:postOfficeBoxNumber |
| AddressLine1(HouseNumber and Street)      | 300       |      |                             | schema:streetAddress       |
| AddressLine2(area)                        | 300       |      |                             | "                          |
| PostTown                                  | 50        |      |                             | schema:addressLocality     |
| County(region)                            | 50        |      |                             | schema:addressRegion       |
| Country                                   | 50        |      |                             | schema:addressCountry      |
| PostCode                                  | 20        |      |                             | schema:postalCode          |
| -                                         |           |      |                             |                            |
| CompanyCategory(corporate_body_type_desc) | 100       | P,R  | rov:orgType                 |                            |
| CompanyStatus(action_code_desc)           | 70        | P,R  | rov:orgStatus               |                            |
| CountryofOrigin                           | 50        | P,L  | terms:countryOfOrigin       |                            |
| Dissolution Date                          | 10        |      |                             | *TODO*                     |
| IncorporationDate                         | 10        | P,L  | terms:incorporationDate     |                            |
| **_Accounts_**                            |           | P,R  | terms:accountsSchedule      |                            |
| AccountingRefDay                          | 2         |      |                             | *TODO*                     |
| AccountingRefMonth                        | 2         |      |                             | *TODO*                     |
| -                                         |           |      |                             |                            |
| **_Returns_**                             |           | P,R  | terms:returnsSchedule       |                            |
| NextDueDate                               | 10        |      |                             | *TODO*                     |
| LastMadeUpDate                            | 10        |      |                             | *TODO*                     |
| -                                         |           |      |                             |                            |
| AccountsCategory(accounts_type_desc)      | 30        |      |                             | *TODO*                     |
| NextDueDate                               | 10        |      |                             | *TODO*                     |
| LastMadeUpDate                            | 10        |      |                             | *TODO*                     |
| **_Mortgages_**                           |           | P,R  | terms:mortgages             |                            |
| NumMortCharges                            | 6         |      | above, *Mortgages*          | *TODO*                     |
| NumMortOutstanding                        | 6         |      | "                           | *TODO*                     |
| NumMortPartSatisfied                      | 6         |      | "                           | *TODO*                     |
| NumMortSatisfied                          | 6         |      | "                           | *TODO*                     |
| -                                         |           |      |                             |                            |
| **_SIC Codes_**                           | (max 4)   | P,R  | rov:orgActivity             |                            |
| SICCode1                                  | 170       |      |                             |                            |
| SICCode2                                  | 170       |      |                             |                            |
| SICCode3                                  | 170       |      |                             |                            |
| SICCode4                                  | 170       |      |                             |                            |
| -                                         |           |      |                             |                            |
| **_Limited Partnerships_**                |           |      |                             |                            |
| NumGenPartners                            | 6         |      |                             | *TODO*                     |
| NumLimPartners                            | 6         |      |                             | *TODO*                     |
| -                                         |           |      |                             |                            |
| URI                                       | 47        |      |                             | schema:url                 |
| **_Previous Names_**                      | (max 10)  |      |                             |                            |
| Change of Name Date                       | 10        |      |                             | *TODO*                     |
| Company name (previous)                   | 160       |      |                             | *TODO*                     |
| -                                         |           |      |                             |                            |
| **_Confirmation Statement_**              |           |      |                             |                            |
| ConfStmtNextDueDate                       | 10        |      |                             | *TODO*                     |
| ConfStmtLastMadeUpDate                    | 10        |      |                             | *TODO*                     |
