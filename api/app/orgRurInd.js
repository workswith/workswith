const Feature = require('./Feature')

/**
 * Normalise the rural indicator:
 */
const orgRurIndMap = new Feature([
  ['Megapolis', 0],
  ['Rural', 1]
])

module.exports = orgRurIndMap
