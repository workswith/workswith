const tap = require('tap')
tap.comment('predict the status of a company')

const { gql } = require('apollo-server')
const ApiServer = require('../app/ApiServer')
const ApiClient = require('../app/ApiClient')

const main = async () => {
  const api = new ApiServer()
  const { url, server } = await api.listen()
  tap.same(url, 'http://localhost:4000/')

  const client = new ApiClient(url)

  let data = await client.query({
    query: gql`
      {
        Prediction(
          orgAge: 2,
          orgCategory: "07",
          orgEmployees: 100,
          orgProfitAndLoss: 2000000,
          orgType: "Private limited company - OOD",
          rurInd: "Megapolis"
        ) {
          risk
        }
      }
    `
  })
  tap.same(
    data.data,
    {
      Prediction: {
        __typename: 'Prediction',
        risk: 4
      }
    }
  )
  server.close()
}

main()
