/**
 * Apply a transform in order to normalise the data.
 */
const path = require('path')
const debug = require('debug')('normalise')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const FileReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileReaderFn')
const FileWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const Split = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Split')

const {
  INPUT_PATH,
  NORMALISE_FN_PATH,
  TASK_ID,
  TEMP_PATH
} = process.env

const { NormaliseFn } = require(NORMALISE_FN_PATH)

/**
 * Creates a single pipeline to normalise the data.
 */
const main = async () => {
  const inputFile = path.resolve(INPUT_PATH, `${TASK_ID}.csv`)
  const outputFile = path.resolve(TEMP_PATH, `${TASK_ID}-normalise.csv`)

  debug(`input file '${inputFile}'`)
  debug(`output file '${outputFile}'`)

  const p = Pipeline.create()

  p
  .apply(ParDo.of(new FileReaderFn(inputFile)))
  .apply(ParDo.of(new Split()))
  .apply(ParDo.of(new Csv(true)))
  .apply(ParDo.of(new NormaliseFn()))
  .apply(
    ParDo.of(new FileWriterFn(outputFile))
  )
  await p.run().waitUntilFinish()
  debug('complete')
}

main()
