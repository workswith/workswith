const debug = require('debug')('ElasticSearchSource')
const elasticsearch = require('elasticsearch')

/**
 * An ElasticSearch datasource.
 *
 * The datasource provides a method for each of the types of data it is
 * able to retrieve.
 */
class ElasticSearchSource {
  /**
   * @param {Object} config - Configuration to be passed to ElasticSearch.
   * @param {string|Object|string[]|Object[]} config.host - ElasticSearch server to query.
   */
  constructor(config) {
    this.client = new elasticsearch.Client(config)
  }

  /**
   * Retrieve a list of companies.
   *
   * @param {object} args - The arguments from the query.
   * @param {string} args.companyStatus - The status of the companies to retrieve.
   * @returns {Object[]} A list of companies with the specified status.
   */
   async getCompanies({ companyStatus }) {
    debug.extend('getCompanies')(`about to request companyStatus: ${companyStatus}`)
    let results
    try {
      const response = await this.client.search({
        index: 'datalake-companieshouse-company',
        q: `CompanyStatus:${companyStatus}`
      })
      debug.extend('getCompanies')(`response was: ${JSON.stringify(response)}`)
      results = response.hits.hits.map(t => t._source)
    } catch(err) {
      console.error(err)
      results = null
    }
    debug.extend('getCompanies')(`returning: ${JSON.stringify(results)}`)
    return results
  }

  /**
   * Retrieve a feed of companies. This is the same as the list from getCompanies()
   * but includes a cursor.
   *
   * @param {object} args - The arguments from the query.
   * @param {string} args.companyStatus - The status of the companies to retrieve.
   * @param {string} args.cursor - A cursor that was returned in a previous call to this method.
   * @returns {Object} An object containing a list of companies with the specified
   * and a cursor for retrieving more.
   */
   async getCompaniesFeed({ companyStatus, cursor, size }) {
    debug.extend('getCompaniesFeed')(`about to request companyStatus: ${companyStatus}`)
    let results
    let newCursor
    try {
      let response
      if (cursor) {
        response = await this.client.scroll({
          scrollId: cursor,
          scroll: '1m'
        })
      } else {
        response = await this.client.search({
          index: 'datalake-companieshouse-company',
          q: `CompanyStatus:${companyStatus}`,
          scroll: '1m',
          size
        })
      }
      debug.extend('getCompaniesFeed')(`response was: ${JSON.stringify(response)}`)
      results = response.hits.hits.map(t => t._source)
      newCursor = response._scroll_id
    } catch(err) {
      console.error(err)
      results = null
      newCursor = null
    }
    debug.extend('getCompaniesFeed')(`returning: ${JSON.stringify(results)}`)
    return {
      cursor: newCursor,
      organizations: results
    }
  }

  /**
   * Retrieve a company.
   *
   * @param {object} args - The arguments from the query.
   * @param {string} args.companyNumber - A Companies House number to identify the company to retrieve.
   * @returns {Object[]} A list of companies with the specified company number.
   */
   async getCompany({ companyNumber }) {
    debug.extend('getCompany')(`about to request companyNumber: ${companyNumber}`)
    let results
    try {
      const response = await this.client.search({
        index: 'datalake-companieshouse-company',
        q: `CompanyNumber:${companyNumber}`
      })
      debug.extend('getCompany')(`response was: ${JSON.stringify(response)}`)
      results = response.hits.hits.map(t => t._source)
    } catch(err) {
      console.error(err)
      results = null
    }
    debug.extend('getCompany')(`returning: ${JSON.stringify(results)}`)
    return results
  }

  /**
   * Retrieve a postcode unit.
   *
   * @param {object} args - The arguments from the query.
   * @param {string} args.postCode - A postcode.
   * @returns {Object[]} A list of postcode units with the specified postcode.
   */

  async getPostCodeUnit({ postCode }) {
    debug.extend('getPostCodeUnit')(`about to request postCode: ${postCode}`)
    let result
    try {
      const response = await this.client.search({
        index: 'datalake-ons-postcode',
        type: 'PostCode',
        q: `pcds:"${postCode}"`
      })
      debug.extend('getPostCodeUnit')(`response was: ${JSON.stringify(response)}`)
      result = response.hits.hits.map(t => t._source)[0]
    } catch(err) {
      console.error(err)
      result = null
    }
    debug.extend('getPostCodeUnit')(`returning: ${JSON.stringify(result)}`)
    return result
  }
}

module.exports = ElasticSearchSource
