import { html, LitElement } from '@polymer/lit-element';

import { SharedStyles } from './shared-styles.js';
import '../../node_modules/ag-grid-community/dist/ag-grid-community.min.noStyle.js';
import '../../node_modules/ag-grid-webcomponent/src/agGrid.js';

// These are the elements needed by this element.
import { connect } from 'pwa-helpers/connect-mixin.js';
import { predictions } from './predictions.js';
import './org-category.js';
import './org-type.js';
import './org-rurind.js';

const monthCellClassRules = {
    'no-risk': 'x === \'none\'',
    'very-low-risk': 'x === \'very low\'',
    'low-risk': 'x === \'low\'',
    'medium-risk': 'x === \'medium\'',
    'high-risk': 'x === \'high\''
};

export class PredictElement extends LitElement {
  async _fetchGraphQl(query) {
    // const origin = 'http://localhost:4000/'
    const origin = 'http://docker-sw-external-9evwo26kh2xq-2147021256.us-east-1.elb.amazonaws.com:4000'
    const url = `${origin}/`

    const response = await fetch(
      url,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'Accept-Encoding': 'gzip, deflate, br',
          Connection: 'keep-alive',
          DNT: 1,
          Origin: origin
        },
        body: JSON.stringify({ query })
      }
    )
    const data = await response.json()
    return data
  }

  _getPrediction(orgAge, orgCategory, orgEmployees, orgProfitAndLoss, orgType, rurInd) {
    return this._fetchGraphQl(`
      query {
        Prediction(
          orgAge: ${orgAge},
          orgCategory: "${orgCategory}",
          orgEmployees: ${orgEmployees},
          orgProfitAndLoss: ${orgProfitAndLoss},
          orgType: "${orgType}",
          rurInd: "${rurInd}"
        ) {
          risk
        }
      }
    `)
  }

  constructor() {
    super()

    this.columnDefs = [
      {
        headerName: 'Name',
        headerTooltip: 'Company Name',
        field: 'name'
      },
      {
        headerName: 'Registered',
        headerTooltip: 'Registration Year',
        field: 'orgRegistrationYear'
      },
      {
        headerName: 'Rural Ind.',
        headerTooltip: 'Urban/Rural Indicator',
        cellEditorSelector: params => {
          return {
            component: 'agSelectCellEditor',
            params: {
              values: [
                'Megapolis',
                'Rural'
              ]
            }
          }
        },
        field: 'rurInd'
      },
      {
        headerName: 'Type',
        headerTooltip: 'Company Type',
        cellEditorSelector: params => {
          return {
            component: 'agSelectCellEditor',
            params: {
              values: [
                'Public limited company - AD',
                'One-person public limited company - EAD',

                'Private limited company - OOD',
                'One-person private limited company - EOOD',

                'Sole proprietorship - ET',

                'Limited partnership',
                'General partnership',

                'Cooperative'
              ]
            }
          }
        },
        field: 'orgType'
      },
      {
        headerName: 'Category',
        headerTooltip: 'Company Category',
        field: 'orgCategory'
      },
      {
        headerName: 'Employees',
        headerTooltip: 'Latest Available Employee Count',
        field: 'orgEmployees'
      },
      {
        headerName: 'Profit and Loss',
        headerTooltip: 'Latest Available Profit and Loss',
        field: 'orgProfitAndLoss'
      },
      {
        headerName: 'Risk',
        headerTooltip: 'Risk Level',
        editable: false,
        field: 'riskLevel',
        cellClassRules: monthCellClassRules
      },
      {
        headerName: 'Av. Risk',
        headerTooltip: 'Industry Average Risk Level',
        editable: false,
        field: 'industryAverageRiskLevel',
        cellClassRules: monthCellClassRules
      }
    ];

    this.rowData = [
      { name: 'Enter Name' },
      { name: 'Enter Name' },
      { name: 'Enter Name' },
      { name: 'Enter Name' },
      { name: 'Enter Name' }
    ]
  }

  firstUpdated() {
    const gridOptions = {
      columnDefs: this.columnDefs,
      defaultColDef: {
        editable: true
      },
      rowData: this.rowData,
      onFirstDataRendered: function () {
        gridOptions.api.sizeColumnsToFit()
      },
      onCellValueChanged: async ev => {
        if (ev.column.colId !== 'riskLevel') {
          const year = new Date().getFullYear()
          const rowNode = ev.node;
          const { orgRegistrationYear, orgCategory, orgEmployees, orgProfitAndLoss, orgType, rurInd } = rowNode.data
          if (orgRegistrationYear && orgCategory && orgEmployees && orgProfitAndLoss && orgType && rurInd) {
            const prediction = await this._getPrediction(
              year - orgRegistrationYear,
              orgCategory,
              orgEmployees,
              orgProfitAndLoss,
              orgType,
              rurInd
            )
            const risk = prediction.data.Prediction.risk

            let riskLevel
            if (risk > 75) {
              riskLevel = 'high'
            } else if (risk > 50) {
              riskLevel = 'medium'
            } else if (risk > 25) {
              riskLevel = 'low'
            } else if (risk > 5) {
              riskLevel = 'very low'
            } else {
              riskLevel = 'none'
            }
            rowNode.setDataValue('riskLevel', riskLevel);
          }
        }
      }
    }
    const agGrid = this.shadowRoot.getElementById('agGrid')
    agGrid.gridOptions = gridOptions
  }

  render() {
    return html`
      ${SharedStyles}
      <style>
        ag-grid {
          padding: 12px;
          border: solid gainsboro 1px;
          height: 240px;
        }
      </style>
      <link rel='stylesheet' href='../../node_modules/ag-grid-community/dist/styles/ag-grid.css'>
      <link rel='stylesheet' href='../../node_modules/ag-grid-community/dist/styles/ag-theme-balham.css'>
      <link rel='stylesheet' href='/src/components/style-risk.css'>
      <ag-grid
       id='agGrid'
       class='ag-theme-balham'
      ></ag-grid>
    `
  }
}

window.customElements.define('predict-element', PredictElement)
