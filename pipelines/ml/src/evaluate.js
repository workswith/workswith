/**
 * Evaluate a model using evaluation data.
 */
const path = require('path')
const debug = require('debug')('evaluate')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const FileReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileReaderFn')
const FileWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const Split = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Split')

const tf = require('@tensorflow/tfjs')
require('@tensorflow/tfjs-node')

const {
  HYPER_LEARNING_RATE,
  HYPER_LOSS,
  MODEL_PATH,
  NORMALISE_FN_PATH,
  TASK_ID,
  TEMP_PATH
} = process.env

const { featureColumns } = require(NORMALISE_FN_PATH)

/**
 * Creates a single pipeline to evaluate the model.
 */
const main = async () => {
  const inputFile = path.join(TEMP_PATH, `${TASK_ID}-partition-evaluate.csv`)
  const modelFile = `file://${MODEL_PATH}/model.json`
  const outputFile = path.join(TEMP_PATH, `${TASK_ID}-evaluate.csv`)

  debug(`input file '${inputFile}'`)
  debug(`model file '${modelFile}'`)
  debug(`output file '${outputFile}'`)

  const p = Pipeline.create()

  p
  .apply(ParDo.of(new FileReaderFn(inputFile)))
  .apply(ParDo.of(new Split()))
  .apply(ParDo.of(new Csv(true)))
  .apply(ParDo.of(new class extends DoFn {
    setup() {
      this.features = []
      this.labels = []
    }

    processElement(c) {
      const input = c.element()

      /**
       * Establish the feature columns for the inputs:
       */
      const features = featureColumns(input)

      this.labels.push(features[0])
      this.features.push(features[1])
    }

    async finishBundle(fbc) {

      /**
       * Create the x's as a 2-D tensor from the features:
       */
      const xs = tf.tensor2d(this.features)

      /**
       * Create the y's as a one-hot from the labels:
       *
       * Note that the type of the 1-D tensor must be int32.
       */
      const labelsTensor = tf.tensor1d(this.labels, 'int32')
      const ys = tf.oneHot(labelsTensor, 2)
      labelsTensor.dispose()

      const model = await tf.loadModel(modelFile)

      /**
       * The optimizer.
       */
      const optimizer = tf.train.adam(+HYPER_LEARNING_RATE)
      /**
       * Compile the model:
       */
      model.compile({
        optimizer,
        loss: HYPER_LOSS,
        metrics: ['accuracy']
      })

      const result = model.evaluate(xs, ys)
      const loss = await result[0].data()
      const accuracy = await result[1].data()
      fbc.output('loss,accuracy\n')
      fbc.output(`${loss},${accuracy}\n`)
    }
  }))
  .apply(
    ParDo.of(new FileWriterFn(outputFile))
  )
  await p.run().waitUntilFinish()
  debug('complete')
}

main()
