/**
 * Apply a full-pass transform in order to partition the data.
 */
const fs = require('fs')
const path = require('path')
const debug = require('debug')('partition')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const FileReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileReaderFn')
const FileWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const Split = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Split')

const {
  TASK_ID,
  TEMP_PATH
} = process.env

class MultiFileWriterFn extends DoFn {
  constructor(prefix, partitions, partitionFn) {
    super()
    this.partitions = partitions
    this.partitionFn = partitionFn
    this.prefix = prefix
    this.streams = []
  }

  /**
   * Note that there is no need for a teardown() since the default for
   * the writable stream is to auto close:
   */
  setup() {

    /**
     * Create an array for the streams:
     */
    for (let partition of this.partitions) {
      const stream = fs.createWriteStream(`${this.prefix}${partition}`)
      this.streams.push(stream)
    }
  }

  processElement(c) {
    const input = c.element()
    /**
     * Work out which partition this entry should go to:
     */
    const partition = this.partitionFn(input)
    debug(`partition: ${partition}`)

    this.streams[partition].write(`${input}\n`)
  }
}

/**
 * Creates a single pipeline to partition the data.
 */
const main = async () => {
  const inputFile = path.resolve(TEMP_PATH, `${TASK_ID}-shuffle.csv`)
  const outputPrefix = path.resolve(TEMP_PATH, `${TASK_ID}-partition`)

  debug(`input file '${inputFile}'`)
  debug(`output prefix '${outputPrefix}'`)

  const p = Pipeline.create()

  p
  .apply(ParDo.of(new FileReaderFn(inputFile)))
  .apply(ParDo.of(new Split()))
  /**
   * Randomly split the input 80/20 into training and evaluation
   * sets.
   */
  .apply(ParDo.of(new MultiFileWriterFn(
    outputPrefix,
    ['-train.csv', '-evaluate.csv'],
    () => Math.random() < 0.8 ? 0 : 1)
  ))
  .apply(
    ParDo.of(new FileWriterFn(outputPrefix))
  )
  await p.run().waitUntilFinish()
  debug('complete')
}

main()
