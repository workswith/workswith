/**
 * Apply a full-pass transform in order to shuffle the data.
 */
const fs = require('fs')
const path = require('path')
const ChildProcess = require('duplex-child-process')
const debug = require('debug')('shuffle')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const FileReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileReaderFn')
const FileWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const Split = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Split')

const {
  TASK_ID,
  TEMP_PATH
} = process.env

class SpawnFn extends DoFn {
  constructor(cmd, args, options) {
    super()
    this.cmd = cmd
    this.args = args
    this.options = options
  }

  setup() {
    this.stream = new ChildProcess.spawn(this.cmd, this.args, this.options)
  }
}

/**
 * Creates a single pipeline to shuffle the data.
 */
const main = async () => {
  const inputFile = path.resolve(TEMP_PATH, `${TASK_ID}-normalise.csv`)
  const outputFile = path.resolve(TEMP_PATH, `${TASK_ID}-shuffle.csv`)

  debug(`input file '${inputFile}'`)
  debug(`output file '${outputFile}'`)

  const p = Pipeline.create()

  p
  .apply(ParDo.of(new FileReaderFn(inputFile)))
  .apply(ParDo.of(new SpawnFn('shuf')))
  .apply(
    ParDo.of(new FileWriterFn(outputFile))
  )
  await p.run().waitUntilFinish()
  debug('complete')
}

main()
