const tap = require('tap')
tap.comment('can launch default server')

const { ApolloServer, gql, makeExecutableSchema} = require('apollo-server')
const { ApolloClient } = require('apollo-client')
const { InMemoryCache } = require('apollo-cache-inmemory')
const { HttpLink } = require('apollo-link-http')
const fetch = require('node-fetch')

const client = new ApolloClient({
  link: new HttpLink({ uri: 'http://localhost:4000', fetch }),
  cache: new InMemoryCache()
})

const main = async () => {
  const typeDefs = gql`
    type Query {
      "A simple type for getting started!"
      hello: String
    }
  `

  const resolvers = {
    Query: {
      hello: () => 'world',
    }
  }

  const schema = makeExecutableSchema({
    typeDefs,
    resolvers
  })

  const api = new ApolloServer({ schema })

  const { url, server } = await api.listen()
  tap.same(url, 'http://localhost:4000/')

  const data = await client.query({ query: gql`{ hello }` })
  tap.same(data.data, { hello: 'world' })
  server.close()
}

main()
