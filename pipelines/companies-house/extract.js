/**
 * Download the zip files from Companies House and extract the CSV
 * files to a temporary directory.
 */
const path = require('path')
const debug = require('debug')('extract')

const FileWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const RequestReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/RequestReaderFn')
const UnzipReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/UnzipReaderFn')

const waitOn = require('wait-on')

/**
 * Grab environment variables:
 *
 *  - the server that we'll pick the zip files up from;
 *  - the year, month and day to get the files for;
 *  - the number of parts that make up the file:
 */
const SERVER_HOST = process.env.COMPANIES_HOUSE_FILES_URL
const YEAR = process.env.COMPANIES_HOUSE_FILES_YEAR
const MONTH = process.env.COMPANIES_HOUSE_FILES_MONTH
const DAY = process.env.COMPANIES_HOUSE_FILES_DAY
const PARTS = process.env.COMPANIES_HOUSE_FILES_PARTS

/**
 * Creates a single pipeline to download one part of the whole, for a specified
 * month.
 */
const basicCompanyData = async (year, month, day, _part, parts) => {
  const part = _part + 1
  const dbg = debug.extend(`basicCompanyData: ${year}-${month} part ${part}`)

  /**
   * Download and unpack a Zip file from Companies House. The URLs look like this:
   *
   * http://download.companieshouse.gov.uk/BasicCompanyData-2018-09-01-part1_5.zip
   * http://download.companieshouse.gov.uk/BasicCompanyData-2018-09-01-part2_5.zip
   * ...
   * http://download.companieshouse.gov.uk/BasicCompanyData-2018-09-01-part5_5.zip
   */
  const zipFile = `/tmp/risingstar/BasicCompanyData/${year}-${month}/zip/part${part}_${parts}.zip`
  const csvFile = `/tmp/risingstar/BasicCompanyData/${year}-${month}/csv/part${part}_${parts}.csv`

  dbg(`about to download zip file to ${zipFile}`)
  let p = Pipeline.create()

  p
  .apply(
    ParDo.of(
      new RequestReaderFn(
        `${SERVER_HOST}/BasicCompanyData-${year}-${month}-${day}-part${part}_${parts}.zip`
      )
    )
  )
  .apply(
    ParDo.of(new FileWriterFn(zipFile))
  )
  await p.run().waitUntilFinish()
  dbg('download complete')

  dbg('about to unzip zip file to ${csvFile}')
  p = Pipeline.create()

  p
  .apply(
    ParDo.of(
      new UnzipReaderFn(zipFile)
    )
  )
  .apply(
    ParDo.of(new FileWriterFn(csvFile))
  )
  await p.run().waitUntilFinish()
  dbg('unzip complete')
}

const main = async () => {
  debug('about to wait for resources')
  await waitOn({
    resources: [ SERVER_HOST ],
    /**
     * No point in waiting too long:
     */
    timeout: 10 * 1000,
    /**
     * If debug is enabled for this module then use verbose
     * logging in wait-on:
     */
    verbose: debug.enabled
  })
  debug('resources ready')

  /**
   * Create as many pipelines as there are parts for the file:
   */
  for (const i of [...Array(+PARTS).keys()]) {
    try {
      await basicCompanyData(YEAR, MONTH, DAY, i, PARTS)
    } catch(err) {
      console.error(`Error in part ${i} of ${PARTS}: ${err}`)
    }
  }
}

main()
