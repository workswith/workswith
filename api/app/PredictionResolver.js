/**
 * This is a resolver for a Prediction. It requires a data source
 * that knows how to make predictions.
 */

const resolver = {
  /**
   * The 'query' section is how we obtain instances of something.
   * They will usually map to queries on a data source:
   */
  query: {
    Prediction: async (_, args, { dataSources }) => {
      /**
       * Get the prediction details from the relevant data source:
       */
      const pred = await dataSources.query.Prediction(args)
      /**
       * Convert the list of objects to a list of class instances:
       */
      return new dataSources.type.Prediction(pred)
    }
  }
}

module.exports = resolver
