const debug = require('debug')('OrganizationResolver')
/**
 * This is a resolver for a Schema.org Organization. It requires a
 * datasource that can provide an Organization, given an identifier.
 *
 * It also requires methods that can map the various properties.
 */

const organizationResolver = {
  /**
   * The 'query' section is how we obtain instances of something.
   * They will usually map to queries on a data source:
   */
  query: {
    Organization: async (_, args, { dataSources }) => {
      debug.extend('query')(`calling Organization ${args}`)
      /**
       * Get a list of organisations from the relevant data source:
       */
      const orgList = await dataSources.query.Organization(args)
      if (!orgList) {
        console.error(`Failed to load list of organizations in Organization query: ${JSON.stringify(args)}`)
      }
      /**
       * Convert the list of objects to a list of class instances:
       */
      return orgList.map(org => new dataSources.type.Organization(org))
    },
    OrganizationFeed: async (_, args, { dataSources }) => {
      debug.extend('query')(`calling OrganizationFeed ${args}`)
      /**
       * Get a list of organisations from the relevant data source:
       */
      const orgList = await dataSources.query.OrganizationFeed(args)
      if (!orgList) {
        console.error(`Failed to load list of organizations in OrganizationFeed query: ${JSON.stringify(args)}`)
      }
      /**
       * Convert the list of objects to a list of class instances:
       */
      return {
        cursor: orgList.cursor,
        organizations: orgList.organizations.map(org => new dataSources.type.Organization(org))
      }
    },
    Organizations: async (_, args, { dataSources }) => {
      debug.extend('query')(`calling Organizations ${args}`)
      /**
       * Get a list of organisations from the relevant data source:
       */
      const orgList = await dataSources.query.Organizations(args)
      if (!orgList) {
        console.error(`Failed to load list of organizations in Organizations query: ${JSON.stringify(args)}`)
      }
      /**
       * Convert the list of objects to a list of class instances:
       */
      return orgList.map(org => new dataSources.type.Organization(org))
    }
  },
  /**
   * The 'type' section is how we obtain the properties of an
   * instance:
   */
  type: {
    Organization: {
      legalName: (parent, _, { dataSources }) => {
        return parent.legalName
      },
      identifier: (parent, _, { dataSources }) => {
        return parent.identifier
      },
      orgType: (parent, _, { dataSources }) => {
        return parent.orgType
      },
      orgStatus: (parent, _, { dataSources }) => {
        return parent.orgStatus
      },
      registeredAddress: (parent, _, { dataSources }) => {
        return {
          postalCode: parent.postalCode
        }
      }
    },
    Address: {
      postalCode: (parent, _, { dataSources }) => {
        return parent.postalCode
      },
      postCodeUnit: async (parent, _, { dataSources }) => {
        return dataSources.query.PostCodeUnit({ postalCode: parent.postalCode })
      }
    }
  }
}

module.exports = organizationResolver
