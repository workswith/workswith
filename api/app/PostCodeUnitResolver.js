/**
 * This is a resolver for an ONS Post Code Unit. It requires
 * a datasource that can provide a PostCodeUnit object, given
 * a post code string.
 *
 * It also requires methods that can map the various properties.
 */

const resolver = {
  /**
   * The 'query' section is how we obtain instances of something.
   * They will usually map to queries on a data source:
   */
  query: {
    PostCodeUnit: async (_, args, { dataSources }) => {
      /**
       * Get the post code unit details from the relevant data source:
       */
      const pc = await dataSources.query.PostCodeUnit(args)
      /**
       * Convert the list of objects to a list of class instances:
       */
      return new dataSources.type.PostCodeUnit(pc)
    }
  },
  /**
   * The 'type' section is how we obtain the properties of an
   * instance:
   */
  type: {
    PostCodeUnit: {
      ru11Ind: (parent, _, { dataSources }) => {
        return parent.ru11ind
      }
    }
  }
}

module.exports = resolver
