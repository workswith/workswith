const tap = require('tap')

/**
 * This is an object as it would be returnd from CH:
 */

const chCompany = {
  CompanyName: 'ENGINE HOUSE SOFTWARE LIMITED',
  CompanyNumber: '09426790'
}

/**
 * These are the mappings from a CH company property to a Schema.org
 * Organization property:
 */

const dataSources = {
  Organization: {
    mapping: (parent, fieldName) => {
      if (fieldName === 'identifier') {
        return parent.CompanyNumber
      }

      if (fieldName === 'legalName') {
        return parent.CompanyName
      }

      throw new ReferenceError(`There is no mapping for '${fieldName}'`)
    }
  }
}

/**
 * This is a resolver that will map fields using whatever mappers it is
 * given in the dataSources object:
 */

class Resolver {
  constructor(typeName) {
    return new Proxy(
      {},
      {
        get: function(obj, prop) {
          return (parent, _, { dataSources }) => {
            return dataSources[typeName].mapping(parent, prop)
          }
        }
      }
    )
  }
}
const organizationResolver = {
  type: {
    Organization: new Resolver('Organization')
  }
}

const resolvers = {
  ...organizationResolver.type
}

/**
 * Ensure that fields are retrieved correctly:
 */

tap.same(
  resolvers.Organization.identifier(chCompany, null, { dataSources }),
  '09426790'
)
tap.same(
  resolvers.Organization.legalName(chCompany, null, { dataSources }),
  'ENGINE HOUSE SOFTWARE LIMITED'
)

/**
 * Ensure that an error is thrown if we try to access a property that is not
 * part of the schema:
 */

tap.throws(
  () => {
    resolvers.Organization.status(chCompany, null, { dataSources })
  },
  ReferenceError
)
